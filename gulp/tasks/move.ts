import { task, src, dest } from "gulp";

task("modules:move", () => {
  return src("dist/**/*").pipe(dest("node_modules/@kaya"));
});
