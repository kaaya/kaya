import { src, task } from "gulp";
import * as del from "gulp-clean";

task("modules:clean", () => {
  return src("node_modules/@kaya", {
    read: false,
    allowEmpty: true,
  }).pipe(del());
});
