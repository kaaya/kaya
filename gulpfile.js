"use strict";

const path = require("path");

require("ts-node").register({
  project: path.join(__dirname, "gulp/tsconfig.json"),
});

require("./gulp/index");
