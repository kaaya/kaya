import "./polyfills";
import { FastifyInstance, FastifyListenOptions } from "fastify";
import { registerPlugin } from "@kaya/common";

export class Kaya {
  readonly #fastify!: FastifyInstance;
  constructor(fastify: FastifyInstance) {
    this.#fastify = fastify;
  }
  async add(plugin: IKaya.PluginType) {
    await registerPlugin(this.#fastify, plugin);
  }
  async listen(opts: FastifyListenOptions) {
    const url = await this.#fastify.listen(opts);
    return url;
  }
  async start(port: number, host: string = "localhost") {
    const url = await this.#fastify.listen({ port, host });
    return url;
  }
}
