import { SetAttribute, TAttr, TCommon, TObject } from "./schema.decorator";

export type ISchema = new (...args: any[]) => any;

export type TObjectAttr = Omit<TCommon, "$id" | "$ref"> &
  Omit<TObject, "type" | "$ref"> & { schema: ISchema };

const type = "object";

export const JObject = Object.assign(({ schema, ...attr }: TObjectAttr) => {
  const data: TAttr = { type, $ref: "", ...attr };
  const ref = schema[Symbol.metadata].$id as string;
  if (!ref) {
    throw new Error(
      `Wrong schem ${schema.name} provided ! Schema must be decorated with JSchema`
    );
  }
  data.$ref = ref;
  return SetAttribute(data);
}, {});
