export type TCommon = {
  $id?: string;
  $ref?: string;
  required?: boolean;
  description?: string;
  nullable?: boolean;
};

export type TString = {
  type: "string";
  minLength?: number;
  maxLength?: number;
  pattern?: string;
  enum?: string[];
};

export type TNumber = {
  type: "number";
  minimum?: number;
  maximum?: number;
  enum?: number[];
};

export type TObject = {
  type: "object";
  $ref: string;
};

export type TAttr = TCommon & (TString | TNumber | TObject);

export function SetAttribute({ required, ...schema }: TAttr) {
  return function (_: string, context: ClassFieldDecoratorContext) {
    if (!context.metadata?.properties) {
      Object.assign(context.metadata, {
        properties: { [context.name]: schema },
      });
    } else {
      Object.assign(context.metadata.properties, {
        [context.name]: schema,
      });
    }
    if (required) {
      if (Array.isArray(context.metadata.required)) {
        context.metadata.required.push(context.name);
      } else {
        context.metadata.required = [context.name];
      }
    }
  };
}

let MODEL = 0;

export const JSchema = function ($id: string = `M${++MODEL}`) {
  return function (_: Object, context: ClassDecoratorContext) {
    console.info(Object.assign(context.metadata, { $id, type: "object" }));
  };
};
