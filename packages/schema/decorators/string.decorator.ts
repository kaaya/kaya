import { SetAttribute, TCommon, TString } from "./schema.decorator";

export type TStringAttr = Omit<TCommon, "$id" | "$ref"> & Omit<TString, "type">;

const type = "string";

const PATTERN = {
  ALPHANUM: "[a-zA-Z0-9]",
  BASE64: "[a-zA-Z0-9]",
  CREDIT_CARD: "[a-zA-Z0-9]",
  DATA_URI: "[a-zA-Z0-9]",
  DOMAIN: "[a-zA-Z0-9]",
  EMAIL: "[a-zA-Z0-9]",
  GUID: "[a-zA-Z0-9]",
  HEX: "[a-zA-Z0-9]",
  HOSTNAME: "[a-zA-Z0-9]",
  IP: "[a-zA-Z0-9]",
  ISO_DATE: "[a-zA-Z0-9]",
  TOKEN: "[a-zA-Z0-9]",
  URI: "[a-zA-Z0-9]",
};

export const JString = (attr?: TStringAttr) => {
  return SetAttribute({ type, ...(attr ?? {}) });
};
