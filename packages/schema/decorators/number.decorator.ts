import { SetAttribute, TCommon, TNumber } from "./schema.decorator";

export type TNumberAttr = Omit<TCommon, "$id" | "$ref"> & Omit<TNumber, "type">;

const type = "number";

export const JNumber = Object.assign(
  (attr: TNumberAttr = {}) => {
    return SetAttribute({ type, ...attr });
  },
  {
    Integer: (attr: TNumberAttr = {}) => {
      return SetAttribute({ type, ...attr });
    },
  }
);
