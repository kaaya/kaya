export * from "./decorators";

import { JString, JNumber, JObject } from "./decorators";

export const J = {
  String: JString,
  Number: JNumber,
  Object: JObject,
};
