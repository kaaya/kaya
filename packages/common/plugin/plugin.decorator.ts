// import { FastifyInstance } from "fastify";
import { OnLifecycle } from "../hooks";

declare global {
  namespace IKaya {
    export interface PluginType extends Function {
      //   [key: string]: unknown;
      new (...args: any[]): Partial<OnLifecycle>;
    }

    export interface PluginMetadata {
      plugins?: PluginType[];
    }

    export interface PluginData extends PluginMetadata {
      name?: string;
    }
    export interface PluginContext {
      name: string;
      instance: InstanceType<IKaya.PluginType>;
      plugins?: PluginContext[];
    }
  }
}

export function Plugin(metadata: IKaya.PluginMetadata = {}) {
  return <T extends IKaya.PluginType>(
    Cls: T,
    context: ClassDecoratorContext
  ): T => {
    // validate metadata
    context.metadata["name"] = context.name;
    Object.assign(context.metadata, metadata);
    return class extends Cls {
      constructor(...args: any[]) {
        super(...args);
      }
    };
  };
}
