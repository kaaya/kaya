import { join } from "node:path";
import { FastifyInstance, RouteOptions } from "fastify";
import { createRouterContext } from "../router/router.factory";

export function createPluginContext(M: IKaya.PluginType): IKaya.PluginContext {
  const metadata = M[Symbol.metadata] as DecoratorMetadata as IKaya.PluginData;
  const entries: [string, unknown][] = [
    ["instance", new M()],
    ["name", metadata.name],
  ];
  if (metadata.plugins?.length) {
    entries.push(["plugins", metadata.plugins.map(createPluginContext)]);
  }
  if (metadata.routers?.length) {
    entries.push(["routers", metadata.routers.map(createRouterContext)]);
  }
  return Object.fromEntries(entries) as unknown as IKaya.PluginContext;
}

export async function registerPlugin(
  fastify: FastifyInstance,
  plugin: IKaya.PluginType
) {
  await (async function reg(
    fastify: FastifyInstance,
    context: IKaya.PluginContext
  ): Promise<void> {
    await fastify.register(async (f: FastifyInstance) => {
      const log = f.log.child({}, { msgPrefix: `[${context.name}] ` });
      log.info("Registering...");
      if (context.instance.onReady) {
        f.addHook("onReady", () => context.instance.onReady(f));
      }
      if (context.instance.onListen) {
        f.addHook("onListen", () => context.instance.onListen(f));
      }
      if (context.instance.beforeClose) {
        f.addHook("preClose", () => context.instance.beforeClose(f));
      }
      if (context.instance.onClose) {
        f.addHook("onClose", () => context.instance.onClose(f));
      }
      if (context.instance.onRoute) {
        f.addHook("onRoute", (opts) => context.instance.onRoute(f, opts));
      }
      if (context.instance.onRegister) {
        f.addHook("onRegister", (opts) => context.instance.onRegister(f, opts));
      }
      context.plugins?.map(async (c) => reg(fastify, c));
      context.routers?.forEach((router) => {
        router.schemas?.forEach((schema) => {
          if (!fastify.getSchemas()[schema["$id"]]) {
            fastify.addSchema(schema);
          }
        });
        router?.routes?.forEach((route) => {
          route.url = join(router.path, route.url).replace(/\/$/, "") || "/";
          log.info(`Mapped {${route.method}, ${route.url}}`);
          f.route(route as RouteOptions);
        });
      });
      log.info("Registered !");
    });
  })(fastify, createPluginContext(plugin));
}
