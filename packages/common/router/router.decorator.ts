import { RouteOptions } from "fastify";

declare global {
  namespace IKaya {
    export interface RouterType extends Function {
      new (...args: any[]): object;
    }
    export interface RouterMetadata {
      path?: string;
      version?: string;
    }
    export interface RouterData extends RouterMetadata {
      schemas?: unknown[];
      routes?: Record<string | symbol, Partial<RouteOptions>>;
    }
    export interface PluginMetadata {
      routers?: RouterType[];
    }
    export interface PluginContext {
      routers?: RouterContext[];
    }
    export interface RouterContext {
      instance: InstanceType<IKaya.RouterType>;
      path: string;
      version: string;
      routes: Partial<RouteOptions>[];
      schemas: unknown[];
    }
  }
}

export function Router(metadata?: IKaya.RouterMetadata) {
  return <T extends IKaya.RouterType>(
    _: T,
    context: ClassDecoratorContext<T>
  ) => {
    Object.assign(context.metadata, metadata);
  };
}
