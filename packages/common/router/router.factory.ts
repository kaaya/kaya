import { join } from "node:path";

export function createRouterContext(C: IKaya.RouterType): IKaya.RouterContext {
  const metadata = C[Symbol.metadata] as IKaya.RouterData;
  const context: IKaya.RouterContext = {
    instance: new C(),
    path: join("/", metadata.path ?? ""),
    version: metadata.version ?? "",
    schemas: metadata.schemas ?? [],
    routes: metadata.routes ? Object.values(metadata.routes) : [],
  };
  return context;
}
