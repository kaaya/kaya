import { RouteOptions, HTTPMethods, FastifySchema } from "fastify";
import { ISchema } from "@kaya/schema";

export function SetMethodMetadata(method: HTTPMethods, url: string = "/") {
  return function (handler: Function, context: ClassMethodDecoratorContext) {
    context.addInitializer(function () {
      const metadata = context.metadata as IKaya.RouterData;
      const route: RouteOptions = { method, url, handler: handler.bind(this) };
      if (!metadata.routes) {
        metadata.routes = { [context.name]: route };
      } else {
        const routes = metadata.routes;
        if (!routes[context.name]) {
          routes[context.name] = route;
        } else {
          Object.assign(routes[context.name], route);
        }
      }
    });
  };
}

export function Get(url?: string) {
  return SetMethodMetadata("GET", url);
}

export function Post(url?: string) {
  return SetMethodMetadata("POST", url);
}

export function Put(url?: string) {
  return SetMethodMetadata("PUT", url);
}

export function Patch(url?: string) {
  return SetMethodMetadata("PATCH", url);
}

export function Delete(url?: string) {
  return SetMethodMetadata("DELETE", url);
}

export function SetSchema(key: string, Schema: ISchema) {
  return function (_: Function, context: ClassMethodDecoratorContext) {
    context.addInitializer(function () {
      const metadata = context.metadata as Partial<IKaya.RouterData>;
      const schema = Schema[Symbol.metadata];
      if (!metadata.schemas) {
        metadata.schemas = [schema];
      } else {
        metadata.schemas.push(schema);
      }
      if (!metadata.routes) {
        metadata.routes = {
          [context.name]: {
            schema: { [key]: { $ref: `${schema["$id"]}#` } },
          },
        };
      } else {
        const routes = metadata.routes;
        if (!routes[context.name]) {
          routes[context.name] = {
            schema: { [key]: { $ref: `${schema["$id"]}#` } },
          };
        } else {
          Object.assign(routes[context.name], {
            schema: {
              ...(routes[context.name] ?? {}),
              [key]: { $ref: `${schema["$id"]}#` },
            },
          });
        }
      }
    });
  };
}

export function Query(Schema: ISchema) {
  return SetSchema("querystring", Schema);
}

export function Param(Schema: ISchema) {
  return SetSchema("params", Schema);
}

export function Body(Schema: ISchema) {
  return SetSchema("body", Schema);
}

export function Header(Schema: ISchema) {
  return SetSchema("headers", Schema);
}

export const Route = {
  Get,
  Post,
  Put,
  Patch,
  Delete,
  Header,
  Query,
  Param,
  Body,
};
