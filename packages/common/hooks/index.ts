import {
  FastifyInstance,
  FastifyPluginOptions,
  RegisterOptions,
  RouteOptions,
} from "fastify";

type THookFn = (fastify: FastifyInstance) => void | Promise<void>;

export interface OnReady {
  onReady: THookFn;
}

export interface OnListen {
  onListen: THookFn;
}

export interface OnClose {
  onClose: THookFn;
}

export interface BeforeClose {
  beforeClose: THookFn;
}

export interface OnRoute {
  onRoute: (
    fastify: FastifyInstance,
    opts: RouteOptions & { routePath: string; path: string; prefix: string }
  ) => void | Promise<void>;
}

export interface OnRegister {
  onRegister: (
    fastify: FastifyInstance,
    opts: RegisterOptions & FastifyPluginOptions
  ) => void | Promise<void>;
}

export interface OnLifecycle
  extends OnReady,
    OnListen,
    BeforeClose,
    OnClose,
    OnRoute,
    OnRegister {}
