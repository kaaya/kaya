import { Plugin } from "@kaya/common";
import { RoomPlugin } from "./room/room.plugin";

@Plugin({
  plugins: [RoomPlugin],
})
export class AppPlugin {}
