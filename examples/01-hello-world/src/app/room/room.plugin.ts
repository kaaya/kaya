import { Plugin, OnRoute } from "@kaya/common";
import { RoomRouter } from "./room.router";

@Plugin({ routers: [RoomRouter] })
export class RoomPlugin implements OnRoute {
  onRoute(f, opts) {
    console.info(opts);
  }
}
