import { Router, Route } from "@kaya/common";
import { CreateRoomPayload, RoomParam, RoomQuery } from "./room.validators";

@Router({
  path: "/rooms",
})
export class RoomRouter {
  @Route.Get()
  @Route.Query(RoomQuery)
  async list() {
    return {
      message: "Success",
      result: [],
    };
  }

  @Route.Post()
  @Route.Body(CreateRoomPayload)
  async create() {
    return {
      message: "Success",
      result: "RM1",
    };
  }

  @Route.Get(":id")
  @Route.Param(RoomParam)
  async details() {
    return {
      message: "Success",
      result: {},
    };
  }
}
