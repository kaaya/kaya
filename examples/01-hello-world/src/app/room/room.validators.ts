import { J, JSchema } from "@kaya/schema";

// @JSchema()
// class Location {
//   @J.String({
//     required: true,
//     enum: ["Point"],
//   })
//   type!: "Point";
// }

// @JSchema()
// class Address {
//   @JString({
//     required: true,
//   })
//   city!: string;

//   @J.Object({
//     schema: Location,
//   })
//   location!: Location;
// }

// @JSchema()
// class User {
//   @JString({
//     required: true,
//   })
//   name!: string;

//   @J.Object({
//     schema: Address,
//     required: true,
//   })
//   address!: Address;
// }

@JSchema()
export class RoomQuery {
  @J.Number({
    required: false,
    enum: [5, 10, 25, 100],
  })
  page!: number;
}

@JSchema()
export class RoomParam {
  @J.Number({
    required: false,
  })
  id!: number;
}

@JSchema()
export class CreateRoomPayload {
  @J.String({
    required: true,
  })
  name!: string;
}
