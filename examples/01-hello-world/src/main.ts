import fastify from "fastify";
import { Kaya } from "@kaya/core";
import { AppPlugin } from "./app/app.plugin";

const kaya = new Kaya(
  fastify({
    logger: {
      transport: {
        target: "pino-pretty",
        options: {
          colorize: true,
        },
      },
    },
  })
);

kaya
  .add(AppPlugin)
  .then(async () => {
    await kaya.start(3000);
  })
  .catch((err) => {
    console.error(err);
  });
